import math
import sys
from random import *
import os

def selectFiles(numTrain):
    hamOne = []
    hamTwo = []
    spam = []
    #check if divisible by 2
    if numTrain % 2 == 0:
        numTrain = numTrain / 2
    else:
        # add 1 to make the training numbers correct
        # for example: if numTrain = 1197 then (numTrain/2)+1=599 and 599/2 = 299
        # therefore there will be 299 emails in each hamOne and hamTwo and 599 in spam
        numTrain = (numTrain / 2) + 1
    while len(hamOne) != (numTrain / 2):
        num = randint(1, 300)
        if num not in hamOne:
            hamOne.append(num)
    while len(hamTwo) != (numTrain / 2):
        num = randint(1, 300)
        if num not in hamTwo:
            hamTwo.append(num)
    while len(spam) != numTrain:
        num = randint(1, 600)
        if num not in spam:
            spam.append(num)
    return hamOne, hamTwo, spam

def training(hamOne, hamTwo, spam):
    # training sets
    dict = {} #dict elements = {key = WORD,[Number of NonSpam Emails with WORD, Number of Spam emails with WORD]}

    if sys.platform == 'win32':
        filepath = os.getcwd() + "\\email data\\Ham\\300 good emails\\"
    elif sys.platform == 'linux2':
        filepath = os.getcwd() + "/email data/Ham/300 good emails/"
    for ham in hamOne:
        f = filepath + str(ham) + ".txt"
        file = open(f, "r")
        # add words to dictionary
        for line in file:
            for word in line.split():
                if word not in dict:
                    dict.update({word: [1, 0]}) #puts word in dict and set initial ham value to 1
                else:
                    dict[word][0] += 1 #increments the amount of ham emails that have WORD present
        file.close()

    if sys.platform == 'win32':
        filepath = os.getcwd() + "\\email data\\Ham\\301-600 good ones\\"
    elif sys.platform == 'linux2':
        filepath = os.getcwd() + "/email data/Ham/301-600 good ones/"
    for ham in hamTwo:
        f = filepath + str(ham) + ".txt"
        file = open(f, "r")
        for line in file:
            for word in line.split():
                if word not in dict:
                    dict.update({word: [1, 0]})
                else:
                    dict[word][0] += 1
        file.close()

    if sys.platform == 'win32':
        filepath = os.getcwd() + "\\email data\\Spam\\"
    elif sys.platform == 'linux2':
        filepath = os.getcwd() + "/email data/Spam/"
    for s in spam:
        f = filepath + str(s) + ".txt"
        file = open(f, "r")
        for line in file:
            for word in line.split():
                if word not in dict:
                    dict.update({word: [0, 1]})
                else:
                    dict[word][1] += 1
        file.close()

    return dict

def normalize(probHam, probSpam):
    return probHam / float(probHam + probSpam), probSpam / float(probHam + probSpam)

def probability(file, dict, num, numHam, numSpam):
    probHam = math.log10(numHam / float(num))
    probSpam = math.log10(numSpam / float(num))
    maxHam = 0
    maxSpam = 0
    hamArr = [] #will store the logs ham probabilities
    spamArr = [] #will store the logs spam probabilities
    words = [] #words in he email
    for line in file:
        for word in line.split():
            words.append(word)
    for key in dict:
        if key in words:
            # log probablity of each word in email
            hamArr.append(math.log10(((dict[key][0] + 1) / float(numHam))))
            spamArr.append(math.log10(((dict[key][1] + 1) / float(numSpam))))

            if (maxHam > math.log10(((dict[key][0] + 1) / float(numHam)))):
                maxHam = math.log10(((dict[key][0] + 1) / float(numHam)))

            if (maxSpam > math.log10(((dict[key][1] + 1) / float(numSpam)))):
                maxSpam = math.log10(((dict[key][1] + 1) / float(numSpam)))

        else:
            # log probablity of each word not in email
            # ((dict[key][x] + 1) / float(numHam)) can come out as 1. since 1-1=0, taking the log will give an error without the try/except
            try:
                hamArr.append(math.log10((1 - ((dict[key][0] + 1) / float(numHam)))))

                if (maxHam > math.log10((1 - ((dict[key][0] + 1) / float(numHam))))):
                    maxHam = math.log10((1 - ((dict[key][0] + 1) / float(numHam))))
            except:
                hamArr.append(0)
            try:
                spamArr.append(math.log10((1 - ((dict[key][1] + 1) / float(numSpam)))))

                if (maxSpam > math.log10((1 - ((dict[key][1] + 1) / float(numSpam))))):
                    maxSpam = math.log10((1 - ((dict[key][1] + 1) / float(numSpam))))
            except:
                spamArr.append(0)

    # sum exponent log probabilties with the max value subtracted (the max is differentiated from the equation)
    for ham in hamArr:
        probHam = probHam + math.exp(ham - maxHam)
    for spam in spamArr:
        probSpam = probSpam + math.exp(spam - maxSpam)

    # add the max to the log of the sum
    probHam = maxHam + math.log10(probHam)
    probSpam = maxSpam + math.log10(probSpam)

    probHam, probSpam = normalize(probHam, probSpam)

    return probHam, probSpam

def test(hamOne, hamTwo, spam, dict, num):
    # testing sets
    numHam = len(hamOne) + len(hamTwo)
    numSpam = len(spam)
    i = 1 #used to loop throuh emails
    j = 0 #counts number emails that are marked as spam
    truePositive = 0 #T = T
    trueNegative = 0 #F = F
    falsePositive = 0 #F = T
    falseNegative = 0 #T = F

    if sys.platform == 'win32':
        filepath = os.getcwd() + "\\email data\\Ham\\300 good emails\\"
    elif sys.platform == 'linux2':
        filepath = os.getcwd() + "/email data/Ham/300 good emails/"
    while i <= 300:
        if i not in hamOne:
            f = filepath + str(i) + ".txt"
            file = open(f, "r")
            probHam, probSpam = probability(file, dict, num, numHam, numSpam)
            if probHam >= probSpam: #greater than or equal to because in the case of a tie the emails should NOT be marked as spam
                truePositive += 1
            else:
                falseNegative += 1
            file.close()
            j += 1
        i += 1

    i = 1
    if sys.platform == 'win32':
        filepath = os.getcwd() + "\\email data\\Ham\\301-600 good ones\\"
    elif sys.platform == 'linux2':
        filepath = os.getcwd() + "/email data/Ham/301-600 good ones/"
    while i <= 300:
        if i not in hamTwo:
            f = filepath + str(i) + ".txt"
            file = open(f, "r")
            probHam, probSpam = probability(file, dict, num, numHam, numSpam)
            if probHam >= probSpam:
                truePositive += 1
            else:
                falseNegative += 1
            file.close()
            j += 1
        i += 1

    print str(falseNegative) + " out of " + str(j) + " not spam emails marked as spam"

    i = 1
    j = 0
    if sys.platform == 'win32':
        filepath = os.getcwd() + "\\email data\\Spam\\"
    elif sys.platform == 'linux2':
        filepath = os.getcwd() + "/email data/Spam/"
    while i <= 600:
        if i not in spam:
            f = filepath + str(i) + ".txt"
            file = open(f, "r")
            probHam, probSpam = probability(file, dict, num, numHam, numSpam)
            if probHam >= probSpam:
                falsePositive += 1
            else:
                trueNegative += 1
            file.close()
            j += 1
        i += 1

    print str(trueNegative) + " out of " + str(j) + " spam emails marked as spam"

    if (truePositive + trueNegative) != 0:
        accuracy = (truePositive + trueNegative) / float(truePositive + trueNegative + falsePositive + falseNegative)
    else:
        accuracy = 0

    if truePositive != 0:
        recall = truePositive / float(truePositive + falseNegative)
        precision = truePositive / float(truePositive + falsePositive)
    else:
        recall = 0
        precision = 0

    if (precision * recall) != 0:
        f1 = 2 * ((precision * recall) / float(precision + recall))
    else:
        f1 = 0

    print "Accuracy: " + str(accuracy)
    print "Recall: " + str(recall)
    print "Precision: " + str(precision)
    print "F1: " + str(f1)

if __name__ == '__main__':
    numTrain = raw_input('How many emails would you like to train (Enter an integer 6 - 1197)?')
    try:
        numTrain = int(numTrain)
    except:
        print "Invalid input, value will be set to 1000."
        numTrain = 1000
    if numTrain < 6 or numTrain > 1197:
        print "Invalid input, value will be set to 1000."
        numTrain = 1000

    hamOne, hamTwo, spam = selectFiles(numTrain)
    dict = training(hamOne, hamTwo, spam) #dict elements = {key = WORD,[Number of NonSpam Emails with WORD, Number of Spam emails with WORD]}
    test(hamOne, hamTwo, spam, dict, numTrain)
